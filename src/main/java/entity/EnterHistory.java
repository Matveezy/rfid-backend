package entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "enter_history")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnterHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime localDateTime;

    @JsonIgnoreProperties("enterHistory")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public EnterHistory(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
